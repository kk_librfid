   1              	# 1 "lib/setbit.S"
   1              	/*
   0              	
   0              	
   2              	 *  linux/arch/arm/lib/setbit.S
   3              	 *
   4              	 *  Copyright (C) 1995-1996 Russell King
   5              	 *
   6              	 * This program is free software; you can redistribute it and/or modify
   7              	 * it under the terms of the GNU General Public License version 2 as
   8              	 * published by the Free Software Foundation.
   9              	 */
  10              	#include <asm/linkage.h>
   1              	#ifndef __ASM_LINKAGE_H
  11              	#include <asm/assembler.h>
   1              	/*
   2              	 *  linux/include/asm-arm/assembler.h
   3              	 *
   4              	 *  Copyright (C) 1996-2000 Russell King
   5              	 *
   6              	 * This program is free software; you can redistribute it and/or modify
   7              	 * it under the terms of the GNU General Public License version 2 as
   8              	 * published by the Free Software Foundation.
   9              	 *
  10              	 *  This file contains arm architecture specific defines
  11              	 *  for the different processors.
  12              	 *
  13              	 *  Do not include any C declarations in this file - it is included by
  14              	 *  assembler source.
  15              	 */
  16              	#ifndef __ASSEMBLY__
  17              	#error "Only include this from assembly code"
  18              	#endif
  19              	
  20              	#include <asm/ptrace.h>
   1              	/*
  21              	
  22              	#define pull            lsl
  23              	#define push            lsr
  24              	#define get_byte_0	lsr #24
  25              	#define get_byte_1	lsr #16
  26              	#define get_byte_2	lsr #8
  27              	#define get_byte_3      lsl #0
  28              	#define put_byte_0	lsl #24
  29              	#define put_byte_1	lsl #16
  30              	#define put_byte_2	lsl #8
  31              	#define put_byte_3      lsl #0
  32              	
  33              	#define PLD(code...)
  34              	
  35              	#define MODE_USR	USR_MODE
  36              	#define MODE_FIQ	FIQ_MODE
  37              	#define MODE_IRQ	IRQ_MODE
  38              	#define MODE_SVC	SVC_MODE
  39              	
  40              	#define DEFAULT_FIQ	MODE_FIQ
  41              	
  42              	/*
  43              	 * LOADREGS - ldm with PC in register list (eg, ldmfd sp!, {pc})
  44              	 */
  45              	#ifdef __STDC__
  46              	#define LOADREGS(cond, base, reglist...)\
  47              		ldm##cond	base,reglist
  48              	#else
  49              	#define LOADREGS(cond, base, reglist...)\
  50              		ldm/**/cond	base,reglist
  51              	#endif
  52              	
  53              	/*
  54              	 * Build a return instruction for this processor type.
  55              	 */
  56              	#define RETINSTR(instr, regs...)\
  57              		instr	regs
  58              	
  59              	/*
  60              	 * Enable and disable interrupts
  61              	 */
  62              		.macro	disable_irq
  63              		msr	cpsr_c, #PSR_I_BIT | SVC_MODE
  64              		.endm
  65              	
  66              		.macro	enable_irq
  67              		msr	cpsr_c, #SVC_MODE
  68              		.endm
  69              	
  70              	/*
  71              	 * Save the current IRQ state and disable IRQs.  Note that this macro
  72              	 * assumes FIQs are enabled, and that the processor is in SVC mode.
  73              	 */
  74              		.macro	save_and_disable_irqs, oldcpsr
  75              		mrs	\oldcpsr, cpsr
  76              		disable_irq
  77              		.endm
  78              	
  79              	/*
  80              	 * Restore interrupt state previously stored in a register.  We don't
  81              	 * guarantee that this will preserve the flags.
  82              	 */
  83              		.macro	restore_irqs, oldcpsr
  84              		msr	cpsr_c, \oldcpsr
  85              		.endm
  86              	
  87              	/*
  88              	 * These two are used to save LR/restore PC over a user-based access.
  89              	 * The old 26-bit architecture requires that we do.  On 32-bit
  90              	 * architecture, we can safely ignore this requirement.
  91              	 */
  92              		.macro	save_lr
  93              		.endm
  94              	
  95              		.macro	restore_pc
  96              		mov	pc, lr
  97              		.endm
  98              	...
  12              	#include "bitops.h"
   1              		.macro	bitop, instr
   2              		and	r2, r0, #7
   3              		mov	r3, #1
   4              		mov	r3, r3, lsl r2
   5              		save_and_disable_irqs ip
   6              		ldrb	r2, [r1, r0, lsr #3]
   7              		\instr	r2, r2, r3
   8              		strb	r2, [r1, r0, lsr #3]
   9              		restore_irqs ip
  10              		mov	pc, lr
  11              		.endm
  12              	
  13              	/**
  14              	 * testop - implement a test_and_xxx_bit operation.
  15              	 * @instr: operational instruction
  16              	 * @store: store instruction
  17              	 *
  18              	 * Note: we can trivially conditionalise the store instruction
  19              	 * to avoid dirting the data cache.
  20              	 */
  21              		.macro	testop, instr, store
  22              		add	r1, r1, r0, lsr #3
  23              		and	r3, r0, #7
  24              		mov	r0, #1
  25              		save_and_disable_irqs ip
  26              		ldrb	r2, [r1]
  27              		tst	r2, r0, lsl r3
  28              		\instr	r2, r2, r0, lsl r3
  29              		\store	r2, [r1]
  30              		restore_irqs ip
  31              		moveq	r0, #0
  32              		mov	pc, lr
  33              		.endm
  34              	...
  13              			.text
  14              	
  15              	/*
  16              	 * Purpose  : Function to set a bit
  17              	 * Prototype: int set_bit(int bit, void *addr)
  18              	 */
  19              	ENTRY(_set_bit_be)
  20 0000 180020E2 			eor	r0, r0, #0x18		@ big endian byte ordering
  21              	ENTRY(_set_bit_le)
  22 0004 072000E2 		bitop	orr
  22      0130A0E3 
  22      1332A0E1 
  22      00C00FE1 
  22      93F021E3 
DEFINED SYMBOLS
        lib/setbit.S:19     .text:0000000000000000 _set_bit_be
        lib/setbit.S:20     .text:0000000000000000 $a
        lib/setbit.S:21     .text:0000000000000004 _set_bit_le

NO UNDEFINED SYMBOLS
